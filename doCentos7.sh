# You must be root to do this

yum update && yum upgrade
yum -y install yum-utils
yum -y install net-tools
yum -y install vim-enhanced
echo "alias vi='vim'" >> /etc/profile
yum -y install wget
yum -y install bind-utils
sed -i 's/#PermitRootLogin yes/PermitRootLogin no/g' /etc/ssh/sshd_config
systemctl reload sshd
gpasswd -a bef wheel

# copy hosts
cp hosts /etc/hosts
cp ifcfg-eth0 /etc/sysconfig/network-scripts/ifcfg-eth0
